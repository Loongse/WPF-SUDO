﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using soudu.sudo;
using System.Windows.Threading;
namespace soudu
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private static int[,] data;//用于接收生成的数独数组
        private static int[,] playdata;//存储玩家进行游戏时的数据
        private static Button[,] gamebtns = new Button[9, 9];//按钮数组
        private static Button[] dialogBtns = new Button[9];
        private Button curBtn;//当前选中的按钮

        //按钮的背景颜色（254, 49, 93）
        private SolidColorBrush preBag;//之前选中按钮的背景颜色

        
        //计时
        private System.Diagnostics.Stopwatch stopwatchGame = new System.Diagnostics.Stopwatch();
        //游戏过程读秒
        private DispatcherTimer gameTimer = new DispatcherTimer();
        public MainWindow()
        {
            InitializeComponent();
            //新游戏计时器
            gameTimer.Interval = new TimeSpan(0, 0, 1);
            gameTimer.Tick += gameTimerTick;
        }

        private void gameTimerTick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            //记录游戏开始时间的计时器
            timeLabel.Text = TimeFormat.timeFormat((int)stopwatchGame.Elapsed.TotalSeconds);//设置开始时间

        }

        private void windowLoaded(object sender, RoutedEventArgs e)
        {
            chooseLevel.SelectedIndex = 0;
            
            for (int i = 0; i < dialogBtns.Length; i++)
            {
                dialogBtns[i] = new Button();
                dialogBtns[i].Content = (i+1).ToString();
                dialogBtns[i].Width = 38;
                dialogBtns[i].Height = 38;
                dialogBtns[i].Click += dialogBtnsClick;
                if (i % 2 == 0) 
                    dialogBtns[i].Background = new SolidColorBrush(Color.FromRgb(239, 242, 132));
                else
                    dialogBtns[i].Background = new SolidColorBrush(Color.FromRgb(255, 204, 0));
                dialogGrid.Children.Add(dialogBtns[i]);
            }
            addBtns();//在窗口加载时加载好一些按钮资源，减少后期开销
        }

        //实现数独数字修改
        private void dialogBtnsClick(object sender, RoutedEventArgs e)
        {
            if (curBtn != null)//如果当前选中的按钮不为空，则赋值
            {
                curBtn.Content = ((Button)sender).Content;
            }
            //bool isSuccess = sudoJudge.isSudoTure(gamebtns, data);
            //if (isSuccess)
            //{
            //    MessageBox.Show("成功解出来了。。。");
            //    //清除当前游戏数据并退出
            //    foreach (Button btn in gamebtns)
            //    {
            //        btn.Content = "";
            //        btn.IsEnabled = false;
            //    }
            //}
        }

        //游戏九宫格内按钮点击事件
        private void gameBtnsClick(object sender, RoutedEventArgs e)
        {
            if (curBtn == null)//当前按钮为空时
            {
                preBag = (SolidColorBrush)((Button)sender).Background;//保存当前的背景
                curBtn = (Button)sender;
                curBtn.Background = new SolidColorBrush(Color.FromRgb(202, 81, 0));//选中按钮的背景
            }
            else if((Button)sender != curBtn)//当选择的按钮改变时
            {
                curBtn.Background = preBag;//当更改选中的按钮时
                preBag = (SolidColorBrush)((Button)sender).Background;//保存当前的背景
                curBtn = (Button)sender;
                curBtn.Background = new SolidColorBrush(Color.FromRgb(202, 81, 0));
            }
           
        }
        //为游戏区域添加按钮
        private void addBtns()
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    gamebtns[i, j] = new Button();
                    if ((i > 2 && i < 6) || (j > 2 && j < 6))
                    {
                        gamebtns[i, j].Background = new SolidColorBrush(Color.FromRgb(55, 138, 74));
                    }
                    if (((i > 2 && i < 6) && (j > 2 && j < 6)))
                    {
                        gamebtns[i, j].Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                    }
                    
                    gamebtns[i, j].FontSize = 20;
                    gamebtns[i, j].Width = 38;
                    gamebtns[i, j].Height = 38;
                    gamebtns[i, j].IsEnabled = false;//未初始化禁止点击
                    gameGrid.Children.Add(gamebtns[i, j]);
                }
            }
        }
        //开始新游戏
        private void newGame(object sender, RoutedEventArgs e)
        {
            //开始计时
            gameTimer.Stop();//暂停计时器
            stopwatchGame.Restart();//重启计时器
            stopwatchGame.Stop();
            stopwatchGame.Start();//重新计时
            gameTimer.Start();

            //初始化按钮
            Sudo sudo = new Sudo();
            //通过选择的难度获取对应的数组数据
            data = sudo.getData();
            string plevel = ((ComboBoxItem)chooseLevel.SelectedValue).Content.ToString();

            //获取生成的游戏数据
            playdata = NewGame.getNewGame(data,plevel);

            //为按钮赋值
            int i = 0, j = 0;
            foreach (Button btn in gamebtns)
            {
                btn.IsEnabled = true;//游戏开始后允许点击
                gamebtns[i, j].Click -= gameBtnsClick;//先解绑事件
                //获取数据
                if (playdata[i, j] == 0)
                {
                    btn.Content = "";
                    gamebtns[i, j].Click += gameBtnsClick;//为按钮添加点击事件,可以将事件绑定延迟到初始化，已经赋值的按钮不可修改，其他按钮可以修改
                }
                else
                {
                    btn.Content = playdata[i, j];
                    //gamebtns[i, j].Background = new SolidColorBrush(Color.FromRgb(254, 49, 93));
                }
                j++;
                if (j == 9)
                {
                    j = 0;
                    i++;
                }
            }
        }
        //退出游戏
        private void exitGame(object sender, RoutedEventArgs e)
        {
            //清除当前游戏数据并退出
            foreach (Button btn in gamebtns)
            {
                btn.Content = "";
                btn.IsEnabled = false;
            }
        }
    }
}
