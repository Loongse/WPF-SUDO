﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//用于判断游戏状态：
//1、格子未填充完：gaming
//2、格子填充完：1、失败 2、成功
    //调用时间：每一次点击修改九宫格内数据时
    //传入参数：int 类型的二维数组，通过判断这个二维数组是否满足条件
namespace soudu.sudo
{
    class sudoJudge
    {
        //可以通过gameData的大小进行扩展
        public static Boolean isSudoTure(Button[,] btns,int[,] data)
        {
            for (int i = 0; i < data.GetLength(0); i++)
            {
                for (int j = 0; j < data.GetLength(1); j++)
                {
                    int btn = ((string)btns[i, j].Content=="")?0:(int)btns[i, j].Content;
                    if (btn != data[i,j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
