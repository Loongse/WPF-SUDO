﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace soudu.sudo
{
    class NewGame
    {
        public static int[,] getNewGame(int[,] data,string plevel)
        {
            int min,max;// 挖空的数目范围
            if (plevel == "简单")
            {
                min = 40;
                max = 15;
            }
            else if (plevel == "一般")
            {
                min = 60;
                max = 10;
            }
            else
            {
                min = 70;
                max = 10;
            }

            var seed = Guid.NewGuid().GetHashCode();//生成随机数种子
            Random random = new Random(seed);//产生随机数
            int count = random.Next(1,max)+min;//返回需要挖空的格子数
                do
                {
                    var rseed = Guid.NewGuid().GetHashCode();//生成随机数种子
                    Random rrandom = new Random(rseed);//产生随机数
                    int r = rrandom.Next(1, 9);
                    var cseed = Guid.NewGuid().GetHashCode();//生成随机数种子
                    Random crandom = new Random(cseed);//产生随机数
                    int c = crandom.Next(1, 9);
                    data[r, c] = 0;
                } while (count-->0);
            return data;
        }
    }
}
