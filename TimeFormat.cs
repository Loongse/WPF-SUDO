﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//时间模板
namespace soudu
{
    class TimeFormat
    {
        public static string timeFormat(int timeNum)
        {
            string timeString = "";
            int ss = timeNum % 3600;
            int hh = timeNum / 3600;
            int mm = timeNum / 60 - hh * 60;
            if (hh < 10)
                timeString += 0.ToString() + hh +":";
            else
                timeString += hh + ":";
            if (mm < 10)
                timeString += 0.ToString() + mm + ":";
            else
                timeString += mm + ":";
            if (ss < 10)
                timeString += 0.ToString() + ss;
            else
                timeString += ss;
            return timeString;
        }
    }
}
